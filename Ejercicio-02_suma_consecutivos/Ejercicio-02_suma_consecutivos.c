#include <stdio.h>

int suma_consecutivos(int numero_elegido){
    int suma = 0;
    for(int i = 1; i <= numero_elegido; i++){
        suma = suma + i;
    }
    return suma;
}

int main(){
    char mensaje[80] = "El numero ingresado no pertenece al rango entre 1 y 50";
    int numero_elegido = 0;
    printf("Ingresa un número entre 1 y 50: ");
    scanf("%d",&numero_elegido);
    if(numero_elegido >= 1 && numero_elegido <= 50){
        printf("%d",suma_consecutivos(numero_elegido));
    }else{
        printf("%s",mensaje);
    }
    getchar(); //Precionar una tecla para finalizar programa.
    return 0;

}