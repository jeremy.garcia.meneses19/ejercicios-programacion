#include <stdio.h>

float dolar_peso(float dolares){
    float VALOR_PESO = 20.59;
    return dolares * VALOR_PESO;
}

int main(){
    char mensaje[80] = "El numero ingresado tiene que ser mayor o igual a 0";
    float dolares = 0;
    printf("Ingresa la cantidad de dolares que quieres convertir a pesos: ");
    scanf("%f",&dolares);
    if(dolares >= 0){
        printf("%f",dolar_peso(dolares));
    }else{
        printf("%s",mensaje);
    }
    getchar(); //Precionar una tecla para finalizar programa.
    getchar();
    return 0;

}